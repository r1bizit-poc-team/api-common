/**
 * 概　要： api-common
 * 作成日： 2018.09.30
 * 作成者： DHC
 */

import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { LoadingController } from 'ionic-angular';
import { retry, timeout } from 'rxjs/operators';


@Injectable()
export class ApiCommonProvider {

    private className = 'ApiCommonProvider';

    private loader;

    constructor(private http: HttpClient, private loadingCtrl: LoadingController) {
    }

    /**
     * GET リクエスト送信の共通処理
     * @param url: リクエストのpath
     * @param options: リクエストのオプション
     * @param params: パラメータ
     * @param load: ローディングの有無
     * @return リクエストのresult
     */
    public async doGet(path: string, params?: Object, options?: Object, load = true) {

        let methodName = 'doGet';

        this.loader = this.loadingCtrl.create({
            content: 'ロード中',
        });
        if (load) {
            this.loader.present();
        }

        let res;
        try {
            let url = path;
            res = await this.http.get(url, this.buildOpts(params, options)).pipe(
                timeout(20000),
                retry(0)
            ).toPromise();
        } catch (httpError) {
            if (httpError.error && httpError.error.error) {
                throw new BaseError(httpError.error.error.message + "[" + httpError.error.error.code + "]", httpError.status);
            } else {
                throw new BaseError('HTTP通信で予期せぬエラーが発生しました。', httpError.status);
            }
        } finally {
            if (load) {
                this.loader.dismiss();
            }
        }

        return res;
    }

    /**
     * Delete リクエスト送信の共通処理
     * @param url: リクエストのpath
     * @param options: リクエストのパラメータ
     * @param params: パラメータ
     * @return リクエストのresult
     */
    public async doDelete(path: string, params?: Object, options?: Object, load = true) {

        let methodName = 'doDelete';

        this.loader = this.loadingCtrl.create({
            content: 'ロード中',
        });
        if (load) {
            this.loader.present();
        }

        let res;
        try {
            let url = path;
            res = await this.http.delete(url, this.buildOpts(params, options)).pipe(
                timeout(20000),
                retry(0)
            ).toPromise();
        } catch (httpError) {
            if (httpError.error && httpError.error.error) {
                throw new BaseError(httpError.error.error.message + "[" + httpError.error.error.code + "]", httpError.status);
            } else {
                throw new BaseError('HTTP通信で予期せぬエラーが発生しました。', httpError.status);
            }
        } finally {
            if (load) {
                this.loader.dismiss();
            }
        }

        return res;
    }

    /**
     * Post リクエスト送信の共通処理
     * @param url: リクエストのpath
     * @param body: any
     * @param params: パラメータ
     * @param options: リクエストのオプション
     * @return リクエストのresult
     */
    public async doPost(path: string, body: any, params?: Object, options?: Object, load = true) {

        let methodName = 'doPost';

        this.loader = this.loadingCtrl.create({
            content: 'ロード中',
        });
        if (load) {
            this.loader.present();
        }

        let res;
        try {
            let url = path;
            res = await this.http.post(url, body, this.buildOpts(params, options)).pipe(
                timeout(20000),
                retry(0)
            ).toPromise();
        } catch (httpError) {
            if (httpError.error && httpError.error.error) {
                throw new BaseError(httpError.error.error.message + "[" + httpError.error.error.code + "]", httpError.status);
            } else {
                throw new BaseError('HTTP通信で予期せぬエラーが発生しました。', httpError.status);
            }
        } finally {
            if (load) {
                this.loader.dismiss();
            }
        }

        return res;
    }

    /**
     * Put リクエスト送信の共通処理
     * @param url: リクエストのpath
     * @param body: any
     * @param params: パラメータ
     * @param options: リクエストのオプション
     * @return リクエストのresult
     */
    public async doPut(path: string, body: any, params?: any, options?: Object, load = true) {

        let methodName = 'doPut';

        this.loader = this.loadingCtrl.create({
            content: 'ロード中',
        });
        if (load) {
            this.loader.present();
        }

        let res;
        try {
            let url = path;
            res = await this.http.put(url, body, this.buildOpts(params, options)).pipe(
                timeout(20000),
                retry(0)
            ).toPromise();
        } catch (httpError) {
            if (httpError.error && httpError.error.error) {
                throw new BaseError(httpError.error.error.message + "[" + httpError.error.error.code + "]", httpError.status);
            } else {
                throw new BaseError('HTTP通信で予期せぬエラーが発生しました。', httpError.status);
            }
        } finally {
            if (load) {
                this.loader.dismiss();
            }
        }

        return res;
    }

    /**
     * リクエストのオプション作成
     * @param opts: カスタマイズオプション
     * @param params: パラメータ
     * @return オプション
     */
    private buildOpts(params: Object, opts: Object): Object {
        let methodName = 'buildOpts';

        let deOpts = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Ocp-Apim-Subscription-Key': ""
            }),
            observe: 'response',
            reportProgress: false,
            responseType: 'json',
            withCredentials: false
        };

        if (params) {
            deOpts['params'] = params;
        }

        if (!opts) {
            return deOpts;
        }

        for (let key in opts) {
            if (key === 'headers') {
                deOpts['headers'] = this.mergeHeader(deOpts['headers'], opts['headers']);
                continue;
            }
            deOpts[key] = opts[key];
        }

        return deOpts;
    }

    /**
     * ヘッダ作成
     * @param defHeaders: デフォルトヘッダ
     * @param optHeaders: カスタマイズヘッダ
     * @return HTTPヘッダ
     */
    private mergeHeader(defHeaders: HttpHeaders, optHeaders: HttpHeaders): HttpHeaders {
        let methodName = 'mergeHeader';

        let headers = {};
        for (let key of defHeaders.keys()) {
            headers[key] = defHeaders.get(key);
        }

        for (let key of optHeaders.keys()) {
            headers[key] = optHeaders.get(key);
        }

        return new HttpHeaders(headers);
    }
}

class BaseError implements Error {
    message: string;
    name: string;
    status: number;

    constructor(message?: string, status?: number) {
        this.message = message || '';
        this.name = this.constructor.name;
        this.status = status;
    }
}
