"use strict";
/**
 * 概　要： api-common
 * 作成日： 2018.09.30
 * 作成者： DHC
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var operators_1 = require("rxjs/operators");
var ApiCommonProvider = /** @class */ (function () {
    function ApiCommonProvider(http, loadingCtrl) {
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.className = 'ApiCommonProvider';
    }
    /**
     * GET リクエスト送信の共通処理
     * @param url: リクエストのpath
     * @param options: リクエストのオプション
     * @param params: パラメータ
     * @param load: ローディングの有無
     * @return リクエストのresult
     */
    ApiCommonProvider.prototype.doGet = function (path, params, options, load) {
        if (load === void 0) { load = true; }
        return __awaiter(this, void 0, void 0, function () {
            var methodName, res, url, httpError_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        methodName = 'doGet';
                        this.loader = this.loadingCtrl.create({
                            content: 'ロード中'
                        });
                        if (load) {
                            this.loader.present();
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        url = path;
                        return [4 /*yield*/, this.http.get(url, this.buildOpts(params, options)).pipe(operators_1.timeout(20000), operators_1.retry(0)).toPromise()];
                    case 2:
                        res = _a.sent();
                        return [3 /*break*/, 5];
                    case 3:
                        httpError_1 = _a.sent();
                        if (httpError_1.error && httpError_1.error.error) {
                            throw new BaseError(httpError_1.error.error.message + "[" + httpError_1.error.error.code + "]", httpError_1.status);
                        }
                        else {
                            throw new BaseError('HTTP通信で予期せぬエラーが発生しました。', httpError_1.status);
                        }
                        return [3 /*break*/, 5];
                    case 4:
                        if (load) {
                            this.loader.dismiss();
                        }
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/, res];
                }
            });
        });
    };
    /**
     * Delete リクエスト送信の共通処理
     * @param url: リクエストのpath
     * @param options: リクエストのパラメータ
     * @param params: パラメータ
     * @return リクエストのresult
     */
    ApiCommonProvider.prototype.doDelete = function (path, params, options, load) {
        if (load === void 0) { load = true; }
        return __awaiter(this, void 0, void 0, function () {
            var methodName, res, url, httpError_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        methodName = 'doDelete';
                        this.loader = this.loadingCtrl.create({
                            content: 'ロード中'
                        });
                        if (load) {
                            this.loader.present();
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        url = path;
                        return [4 /*yield*/, this.http["delete"](url, this.buildOpts(params, options)).pipe(operators_1.timeout(20000), operators_1.retry(0)).toPromise()];
                    case 2:
                        res = _a.sent();
                        return [3 /*break*/, 5];
                    case 3:
                        httpError_2 = _a.sent();
                        if (httpError_2.error && httpError_2.error.error) {
                            throw new BaseError(httpError_2.error.error.message + "[" + httpError_2.error.error.code + "]", httpError_2.status);
                        }
                        else {
                            throw new BaseError('HTTP通信で予期せぬエラーが発生しました。', httpError_2.status);
                        }
                        return [3 /*break*/, 5];
                    case 4:
                        if (load) {
                            this.loader.dismiss();
                        }
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/, res];
                }
            });
        });
    };
    /**
     * Post リクエスト送信の共通処理
     * @param url: リクエストのpath
     * @param body: any
     * @param params: パラメータ
     * @param options: リクエストのオプション
     * @return リクエストのresult
     */
    ApiCommonProvider.prototype.doPost = function (path, body, params, options, load) {
        if (load === void 0) { load = true; }
        return __awaiter(this, void 0, void 0, function () {
            var methodName, res, url, httpError_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        methodName = 'doPost';
                        this.loader = this.loadingCtrl.create({
                            content: 'ロード中'
                        });
                        if (load) {
                            this.loader.present();
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        url = path;
                        return [4 /*yield*/, this.http.post(url, body, this.buildOpts(params, options)).pipe(operators_1.timeout(20000), operators_1.retry(0)).toPromise()];
                    case 2:
                        res = _a.sent();
                        return [3 /*break*/, 5];
                    case 3:
                        httpError_3 = _a.sent();
                        if (httpError_3.error && httpError_3.error.error) {
                            throw new BaseError(httpError_3.error.error.message + "[" + httpError_3.error.error.code + "]", httpError_3.status);
                        }
                        else {
                            throw new BaseError('HTTP通信で予期せぬエラーが発生しました。', httpError_3.status);
                        }
                        return [3 /*break*/, 5];
                    case 4:
                        if (load) {
                            this.loader.dismiss();
                        }
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/, res];
                }
            });
        });
    };
    /**
     * Put リクエスト送信の共通処理
     * @param url: リクエストのpath
     * @param body: any
     * @param params: パラメータ
     * @param options: リクエストのオプション
     * @return リクエストのresult
     */
    ApiCommonProvider.prototype.doPut = function (path, body, params, options, load) {
        if (load === void 0) { load = true; }
        return __awaiter(this, void 0, void 0, function () {
            var methodName, res, url, httpError_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        methodName = 'doPut';
                        this.loader = this.loadingCtrl.create({
                            content: 'ロード中'
                        });
                        if (load) {
                            this.loader.present();
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        url = path;
                        return [4 /*yield*/, this.http.put(url, body, this.buildOpts(params, options)).pipe(operators_1.timeout(20000), operators_1.retry(0)).toPromise()];
                    case 2:
                        res = _a.sent();
                        return [3 /*break*/, 5];
                    case 3:
                        httpError_4 = _a.sent();
                        if (httpError_4.error && httpError_4.error.error) {
                            throw new BaseError(httpError_4.error.error.message + "[" + httpError_4.error.error.code + "]", httpError_4.status);
                        }
                        else {
                            throw new BaseError('HTTP通信で予期せぬエラーが発生しました。', httpError_4.status);
                        }
                        return [3 /*break*/, 5];
                    case 4:
                        if (load) {
                            this.loader.dismiss();
                        }
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/, res];
                }
            });
        });
    };
    /**
     * リクエストのオプション作成
     * @param opts: カスタマイズオプション
     * @param params: パラメータ
     * @return オプション
     */
    ApiCommonProvider.prototype.buildOpts = function (params, opts) {
        var methodName = 'buildOpts';
        var deOpts = {
            headers: new http_1.HttpHeaders({
                'Content-Type': 'application/json',
                'Ocp-Apim-Subscription-Key': ""
            }),
            observe: 'response',
            reportProgress: false,
            responseType: 'json',
            withCredentials: false
        };
        if (params) {
            deOpts['params'] = params;
        }
        if (!opts) {
            return deOpts;
        }
        for (var key in opts) {
            if (key === 'headers') {
                deOpts['headers'] = this.mergeHeader(deOpts['headers'], opts['headers']);
                continue;
            }
            deOpts[key] = opts[key];
        }
        return deOpts;
    };
    /**
     * ヘッダ作成
     * @param defHeaders: デフォルトヘッダ
     * @param optHeaders: カスタマイズヘッダ
     * @return HTTPヘッダ
     */
    ApiCommonProvider.prototype.mergeHeader = function (defHeaders, optHeaders) {
        var methodName = 'mergeHeader';
        var headers = {};
        for (var _i = 0, _a = defHeaders.keys(); _i < _a.length; _i++) {
            var key = _a[_i];
            headers[key] = defHeaders.get(key);
        }
        for (var _b = 0, _c = optHeaders.keys(); _b < _c.length; _b++) {
            var key = _c[_b];
            headers[key] = optHeaders.get(key);
        }
        return new http_1.HttpHeaders(headers);
    };
    ApiCommonProvider = __decorate([
        core_1.Injectable()
    ], ApiCommonProvider);
    return ApiCommonProvider;
}());
exports.ApiCommonProvider = ApiCommonProvider;
var BaseError = /** @class */ (function () {
    function BaseError(message, status) {
        this.message = message || '';
        this.name = this.constructor.name;
        this.status = status;
    }
    return BaseError;
}());
